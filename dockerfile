FROM cassandra:3.11.4

RUN mkdir opt/jmx_prometheus
ADD "https://repo1.maven.org/maven2/io/prometheus/jmx/jmx_prometheus_javaagent/0.12.0/jmx_prometheus_javaagent-0.12.0.jar" opt/jmx_prometheus
RUN chmod 644 opt/jmx_prometheus/jmx_prometheus_javaagent-0.12.0.jar
ADD cassandra.yml opt/jmx_prometheus/cassandra.yml

# add dependencies for logstash-logback-encoder
#ADD lib /usr/share/cassandra/lib

ENV JVM_OPTS "$JVM_OPTS -javaagent:/prometheus/jmx_prometheus_javaagent-0.12.0.jar=7070:opt/jmx_prometheus/cassandra.yml "

EXPOSE 7070